package main

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
)

type App struct {
	Router *gin.Engine
	DB     *sql.DB
}

func (a *App) Initialize(host, user, password, dbname string) {
	connectionString :=
		fmt.Sprintf("host=%s user=%s password=%s dbname=%s sslmode=disable", host, user, password, dbname)

	var err error
	a.DB, err = sql.Open("postgres", connectionString)
	if err != nil {
		log.Fatal(err)
	}
	err = a.DB.Ping()
	if err != nil {
		log.Fatal(err)
	}

	a.Router = gin.Default()

	a.initializeRoutes()

}

func (a *App) initializeRoutes() {
	a.Router.Use(CORSMiddleware())
	a.Router.GET("/todos", a.getTodos)
	a.Router.POST("/todos", a.postTodo)
}

func (a *App) Run(addr string) {
	log.Fatal(http.ListenAndServe(addr, a.Router))
}

func (a *App) getTodos(c *gin.Context) {
	todos, err := getAllTodos(a.DB)
	if err != nil {
		return
	}
	c.IndentedJSON(http.StatusOK, todos)
}

func (a *App) postTodo(c *gin.Context) {
	var t todo

	if err := c.BindJSON(&t); err != nil {
		return
	}

	if err := t.createTodo(a.DB); err != nil {
		return
	}

	c.IndentedJSON(http.StatusCreated, t)
}

func CORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}
