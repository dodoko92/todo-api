FROM golang:1.17.3-alpine

RUN apk add --no-cache git

WORKDIR /app/todo

COPY go.mod .

RUN go mod download

COPY . .

RUN go build -o ./out/todo .

EXPOSE 9000

CMD ["./out/todo"]