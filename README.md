# Todo GoLang Backend Example with A-TDD Development

## Requirements
- go 1.17.3
- postgresql instance

## How to Run Development
Set environment variables
```bash
export APP_DB_HOST=localhost
export APP_DB_USERNAME=postgres
export APP_DB_PASSWORD=
export APP_DB_NAME=postgres
```
then
```bash
$ go run .
```

## Test Cases

### Run Integrations and Unit Test Cases
```bash
$ go test -v -run main
```

## CDC Cases
* Requires pack-cli tools

### Verify and Publish Provider
```bash
go test -v -run TestProvider
```

### Can I Deploy Frontend?
```bash
pact-broker can-i-deploy --pacticipant todo-frontend --version foo --broker-base-url localhost
```

## Run on Docker
### How to build Docker image
```bash
$ docker build -t todo-api:latest .
```

### How to run built image
```bash
$ docker run -p --env-file .env 9000:9000 todo-api
```


## References

* [Gin Web Framework](https://github.com/gin-gonic/gin)

* [Tutorial: Developing a RESTful API with Go and Gin](https://golang.org/doc/tutorial/web-service-gin)

* [Testing APIs in Golang using apitest](https://dev.to/eminetto/testing-apis-in-golang-using-apitest-1860)

* [ApiTest Github Repo](https://github.com/steinfletcher/apitest)

* [Building and Testing a REST API in Go with Gorilla Mux and PostgreSQL](https://semaphoreci.com/community/tutorials/building-and-testing-a-rest-api-in-go-with-gorilla-mux-and-postgresql)