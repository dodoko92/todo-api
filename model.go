package main

import (
	"database/sql"
)

type todo struct {
	ID   int    `json:"id"`
	Text string `json:"text" binding:"required"`
}

func getAllTodos(db *sql.DB) ([]todo, error) {
	rows, err := db.Query(
		"SELECT id, text FROM todos LIMIT 1000 OFFSET 0")

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	todos := []todo{}

	for rows.Next() {
		var t todo
		if err := rows.Scan(&t.ID, &t.Text); err != nil {
			return nil, err
		}
		todos = append(todos, t)
	}

	return todos, nil
}

func (t *todo) createTodo(db *sql.DB) error {
	err := db.QueryRow(
		"INSERT INTO todos(text) VALUES($1) RETURNING id",
		t.Text).Scan(&t.ID)

	if err != nil {
		return err
	}

	return nil
}
