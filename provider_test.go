package main

import (
	"os"
	"testing"

	"github.com/pact-foundation/pact-go/dsl"
	"github.com/pact-foundation/pact-go/types"
)

func TestProvider(t *testing.T) {

	// Create Pact connecting to local Daemon
	pact := &dsl.Pact{
		Provider: "todo-api",
	}

	// Start provider API in the background
	go main()

	// Verify the Provider using the locally saved Pact Files
	pact.VerifyProvider(t, types.VerifyRequest{
		ProviderBaseURL:            "http://localhost:" + os.Getenv("APP_PORT"),
		BrokerURL:                  "https://todoapptest.pactflow.io",
		BrokerToken:                os.Getenv("PACT_BROKER_TOKEN"),
		ProviderVersion:            os.Getenv("CI_COMMIT_SHORT_SHA"),
		ProviderBranch:             os.Getenv("CI_BRANCH"),
		ProviderTags:               []string{os.Getenv("CI_COMMIT_REF_NAME")},
		PublishVerificationResults: true,
	})
}
