package main

import (
	"log"
	"net/http"
	"os"
	"testing"

	"github.com/steinfletcher/apitest"
)

var a App

func TestMain(m *testing.M) {
	a.Initialize(
		os.Getenv("APP_DB_HOST"),
		os.Getenv("APP_DB_USERNAME"),
		os.Getenv("APP_DB_PASSWORD"),
		os.Getenv("APP_DB_NAME"))

	ensureTableExists()
	code := m.Run()
	clearTable()
	os.Exit(code)
}

const tableCreationQuery = `CREATE TABLE IF NOT EXISTS todos
(
    id SERIAL,
    text TEXT NOT NULL,
    CONSTRAINT todos_pkey PRIMARY KEY (id)
)`

func ensureTableExists() {
	if _, err := a.DB.Exec(tableCreationQuery); err != nil {
		log.Fatal(err)
	}
}

func clearTable() {
	a.DB.Exec("DELETE FROM todos")
	a.DB.Exec("ALTER SEQUENCE todos_id_seq RESTART WITH 1")
}

func addTodo(text string) {
	a.DB.Exec("INSERT INTO todos(text) VALUES($1)", ""+text)
}

func TestGetTodos_NotFound(t *testing.T) {
	clearTable()
	apitest.New().
		Handler(a.Router).
		Get("/todos").
		Expect(t).
		Body(`[]`).
		Status(http.StatusOK).
		End()
}

func TestGetTodos_Success(t *testing.T) {
	clearTable()
	addTodo("buy some milk")
	var expectedResult = `[{"id": 1, "text": "buy some milk"}]`
	apitest.New().
		Handler(a.Router).
		Get("/todos").
		Expect(t).
		Body(expectedResult).
		Status(http.StatusOK).
		End()
}

func TestPostTodos_Success(t *testing.T) {
	clearTable()
	var jsonStr = `{"text": "buy some milk"}`
	var expectedResult = `{"id": 1, "text": "buy some milk"}`

	apitest.New().
		Handler(a.Router).
		Post("/todos").
		Body(jsonStr).
		Expect(t).
		Body(expectedResult).
		Status(http.StatusCreated).
		End()
}

func TestPostTodos_BadRequest(t *testing.T) {
	clearTable()
	var jsonStr = `{"somekey": "buy some milk"}`
	apitest.New().
		Handler(a.Router).
		Post("/todos").
		Body(jsonStr).
		Expect(t).
		Status(http.StatusBadRequest).
		End()
}
